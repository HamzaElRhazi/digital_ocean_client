package DigitalOcean.XLSProcessor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public abstract class XLSProcessor {
	public List<String> processXLSGivenColum(String xlsPath, int columnIndex){
		List<String> values = new ArrayList<>();
		try(FileInputStream fis = new FileInputStream(xlsPath)){
			
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.rowIterator();
            while(rows.hasNext()) {  // For each Row.
            	XSSFRow row = (XSSFRow) rows.next();
                // Skip header
                if(row.getRowNum() ==0) {
                	continue;
                 }
                Iterator<Cell> cells = row.cellIterator();
                while (cells.hasNext()) {
	                 Cell cell = cells.next();
	                 if(cell.getColumnIndex() == columnIndex) {
	                	 values.add(cell.getStringCellValue());
	                 }
                }
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
			System.out.println("I/O Exception!");
		}
		
		return values;
	}
	
	/*
	 * processXLSFirstRow(String xlsPath) method
	 * expected returned ArrayList of String
	 * index 0 ==> region : String
	 * index 1 ==> size : String
	 * index 2 ==> image_id : Numeric
	 * index 2 ==> image slug : String
	 * index 3 ==> backups : boolean
	 * index 4 ==> ipv6 : boolean
	 * index 5 ==> private_networking : boolean
	 * index 6 ==> user_data : String
	 * index 7 ==> monitoring : boolean
	 */
	public List<String> processXLSFirstRow(String xlsPath){
		List<String> values = new ArrayList<>();
		try(FileInputStream fis = new FileInputStream(xlsPath)){
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.rowIterator();
            while(rows.hasNext()) {  // For each Row.
            	XSSFRow row = (XSSFRow) rows.next();
                // Skip header
                if(row.getRowNum() ==0) {
                	continue;
                 }
                Iterator<Cell> cells = row.cellIterator();
                while (cells.hasNext()) {
	                 Cell cell = cells.next();
	                 if(cell.getColumnIndex() == 1) {
	                	 values.add(cell.getStringCellValue());// region
	                 }
	                 if(cell.getColumnIndex() == 2) {
	                	 values.add(cell.getStringCellValue());// size
	                 }
	          
	                 if(cell.getColumnIndex() == 3) {
	                	 values.add(cell.getStringCellValue());// image_slug
	                 }
	                 if(cell.getColumnIndex() == 4) {
	                	 int backups = (int) cell.getNumericCellValue();
	                	 String backups_str = Integer.toString(backups);
	                	 values.add(backups_str);// backups
	                 }
	                 if(cell.getColumnIndex() == 5) {
	                	 int ipv6 = (int) cell.getNumericCellValue();
	                	 String ipv6_str = Integer.toString(ipv6);
	                	 values.add(ipv6_str);// ipv6
	                 }
	                 if(cell.getColumnIndex() == 6) {
	                	 int private_networking = (int) cell.getNumericCellValue();
	                	 String private_networking_str = Integer.toString(private_networking);
	                	 values.add(private_networking_str);// private_Networking
	                 }

	                 if(cell.getColumnIndex() == 7) {
	                	 values.add(cell.getStringCellValue());// user_data
	                 }
	                 
	                 if(cell.getColumnIndex() == 8) {
	                	 int monitoring = (int) cell.getNumericCellValue();
	                	 String monitoring_str = Integer.toString(monitoring);
	                	 values.add(monitoring_str);// monitoring
	                 }
                }
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		return values;	
	}
	
	
	
	
}
