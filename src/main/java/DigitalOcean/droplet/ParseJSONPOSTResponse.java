package DigitalOcean.droplet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

public class ParseJSONPOSTResponse {

	public static void parseJson(String jsonString) {

		JSONObject obj = new JSONObject(jsonString);
		/*
		 * String dropletName = obj.getJSONObject("links").getString("name");
		 * 
		 * System.out.println(dropletName);
		 */

		JSONArray arr = obj.getJSONArray("droplets");

		System.out.format(
				"|------------|----------------------------------------|-------------------|----------------|%n");
		System.out.format(
				"|     ID     |                  NAME                  |     IP ADRESS     |     REGION     |%n");
		System.out.format(
				"|------------|----------------------------------------|-------------------|----------------|%n");
		String leftAlignFormat = "| %-10s | %-38s | %-17s | %-14s |%n";

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Datatypes in Java");
		Row rowHeader = sheet.createRow(0);
		Cell cellHeader = rowHeader.createCell(0);
		cellHeader.setCellValue("ID");

		sheet.autoSizeColumn(0);
		cellHeader = rowHeader.createCell(1);
		cellHeader.setCellValue("NAME");
		sheet.autoSizeColumn(1);
		cellHeader = rowHeader.createCell(2);
		cellHeader.setCellValue("IP ADRESS");
		sheet.autoSizeColumn(2);
		cellHeader = rowHeader.createCell(3);
		cellHeader.setCellValue("REGION");
		sheet.autoSizeColumn(4);

		for (int i = 0; i < arr.length(); i++) {
			long dropletId = arr.getJSONObject(i).getLong("id");
			String dropletName = arr.getJSONObject(i).getString("name");
			JSONObject dropletRegion = arr.getJSONObject(i).getJSONObject("region");
			String dropletRegionslug = dropletRegion.getString("slug");
			String dropletIp_adresse = arr.getJSONObject(i).getJSONObject("networks").getJSONArray("v4")
					.getJSONObject(0).getString("ip_address");

			// **********************************************************************//

			System.out.format(leftAlignFormat, dropletId, dropletName, dropletIp_adresse, dropletRegionslug);

			System.out.format(
					"|------------|----------------------------------------|-------------------|----------------|%n");

			Row row = sheet.createRow(i+1);
			Cell cell = row.createCell(0);
			cell.setCellValue(dropletId);

			sheet.autoSizeColumn(0);
			cell = row.createCell(1);
			cell.setCellValue(dropletName);
			sheet.autoSizeColumn(1);
			cell = row.createCell(2);
			cell.setCellValue(dropletName);
			sheet.autoSizeColumn(2);
			cell = row.createCell(3);
			cell.setCellValue(dropletRegionslug);
			sheet.autoSizeColumn(4);

		}
		try {
			String path = new File("").getAbsoluteFile() + "\\outputs";
			File file = new File(path);
			if (!new File(path).exists()) {
				file.mkdirs();
			}

			FileOutputStream outputStream = new FileOutputStream(path + "\\dropletsCreated.xlsx");
			workbook.write(outputStream);
		} catch (FileNotFoundException e) {
			System.out.println(
					"The excel is not generated. The most probably you need to close the Excel file and retry displaying");
		} catch (IOException e) {
			System.out.println("I/O exception!");
		}

		
		// ****************************
	}

	public static Map<Integer,Integer> parseJsontoGetAvailableDropletMenu(String jsonString) {
		Map<Integer, Integer> mapDropletsToDelete = new HashMap<>();
		JSONObject obj = new JSONObject(jsonString);
		JSONArray arr = obj.getJSONArray("droplets");
		System.out.format(
				"|------|------------|----------------------------------------|-------------------|----------------|%n");
		System.out.format(
				"| Rang |     ID     |                  NAME                  |     IP ADRESS     |     REGION     |%n");
		System.out.format(
				"|------|------------|----------------------------------------|-------------------|----------------|%n");
		String leftAlignFormat = "| %-4d | %-10s | %-38s | %-17s | %-14s |%n";
		for (int i = 0; i < arr.length(); i++) {
			int rang = i;
			int dropletId = arr.getJSONObject(i).getInt("id");
			String dropletName = arr.getJSONObject(i).getString("name");
			JSONObject dropletRegion = arr.getJSONObject(i).getJSONObject("region");
			String dropletRegionslug = dropletRegion.getString("slug");
			String dropletIp_adresse = arr.getJSONObject(i).getJSONObject("networks").getJSONArray("v4")
					.getJSONObject(0).getString("ip_address");

			// **********************************************************************//

			System.out.format(leftAlignFormat,rang ,dropletId, dropletName, dropletIp_adresse, dropletRegionslug);

			System.out.format(
					"|------|------------|----------------------------------------|-------------------|----------------|%n");
			mapDropletsToDelete.put(rang, dropletId);
		}
		return mapDropletsToDelete;
	}


}
