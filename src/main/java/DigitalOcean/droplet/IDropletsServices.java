package DigitalOcean.droplet;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface IDropletsServices {
	public  List<Droplet> addDroplets(DropletsCreationPOJO dropletsCreationPOJO);	
	public void deleteDroplets(int dropletId);
	public void deleteDropletsByTag(String tag);
	public void displayDroplets() throws FileNotFoundException, IOException;
	
}
