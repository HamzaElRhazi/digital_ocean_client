package DigitalOcean.droplet;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import DigitalOcean.Digital_Ocean_Client.App;

//import com.sun.jersey.api.client.WebResource;

/*import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;*/

import DigitalOcean.XLSProcessor.XLSProcessor;


public class DropletsServices extends XLSProcessor implements IDropletsServices {
	
	
	public DropletsCreationPOJO prepareDropletsToAdd() {
		DropletsCreationPOJO dropletsToBeCreated = new DropletsCreationPOJO();
		System.out.println("Please type the path of your excel file that contains Droplets to be created");
		String inputPathFileDropletsToBeCreated;
		Scanner readFileDropletsToBeCreated = new Scanner(System.in);
		inputPathFileDropletsToBeCreated = readFileDropletsToBeCreated.next();
		
		try{List<String> names = processXLSGivenColum(inputPathFileDropletsToBeCreated, 0);
		List<String> tags = processXLSGivenColum(inputPathFileDropletsToBeCreated, 9);
		// read first row in this index interval [1:9]
		List<String> otherProperties = new ArrayList<String>();
		otherProperties = processXLSFirstRow(inputPathFileDropletsToBeCreated);

		boolean backups = false; // TODO convert otherProperties.get(4) into boolean value
		String backups_str = otherProperties.get(3);
		int backups_int = 0;
		try {
			backups_int = Integer.parseInt(backups_str);// Handle Exception here with parseInt

		} catch (NumberFormatException e) {
			System.out.println("Number format exception in backups!");
		}
		if (backups_int == 0)
			backups = false;
		else if (backups_int == 1)
			backups = true;

		boolean ipv6 = true; 
		boolean private_networking = false; // TODO convert otherProperties.get(6) into boolean value
		String private_networking_str = otherProperties.get(5);
		int private_networking_int = 0;
		try {
			private_networking_int = Integer.parseInt(private_networking_str);// Handle Exception here with parseInt

		} catch (NumberFormatException e) {
			System.out.println("Number format exception in private_networking!");
		}
		if (private_networking_int == 0)
			private_networking = false;
		else if (private_networking_int == 1)
			private_networking = true;

		boolean monitoring = false; 
		String monitoring_str = otherProperties.get(7);
		int monitoring_int = 0;
		try {
			monitoring_int = Integer.parseInt(monitoring_str);// Handle Exception here with parseInt

		} catch (NumberFormatException e) {
			System.out.println("Number format exception in monitoring");
		}
		if (monitoring_int == 0)
			monitoring = false;
		else if (monitoring_int == 1)
			monitoring = true;
		
		

		// filling dropletsToBeCreated object with all properties
		dropletsToBeCreated = new DropletsCreationPOJO(names, otherProperties.get(0), otherProperties.get(1),
				otherProperties.get(2), backups, ipv6, private_networking, otherProperties.get(6), monitoring, tags);
		}catch(Exception e ) {
			System.out.println("File not Found at the provided path!");
			App.menu();
		}
		
		
		return dropletsToBeCreated;
	};

	
	public void deleteDroplets(int dropletID) {
		String tokenApi="";
		try (InputStream input = new FileInputStream("parameters.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            tokenApi = prop.getProperty("authorization");

        } catch (IOException ex) {
            System.out.println("verify your token api in properties file!");
        }
		String urlCreateMultipleDroplets = "https://api.digitalocean.com/v2/";// droplets
		Client createDropletsRestClient = ClientBuilder.newClient();
		WebTarget baseTarget = createDropletsRestClient.target(urlCreateMultipleDroplets);		
		WebTarget dropletsTarget = baseTarget.path("droplets/");
		WebTarget idTarget = dropletsTarget.path(""+dropletID);
		Response DeleteResponse =  idTarget.request()
				.header("Authorization", "Bearer " + tokenApi)
				.delete();
		if (DeleteResponse.getStatus() == 204 )
			System.out.println("Droplet Deleted Successfuly");
		else System.out.println("Ad failed! Verify your file data and retry");
	}

	
	public void deleteDropletsByTag(String tag) {
		String tokenApi="";
		try (InputStream input = new FileInputStream("parameters.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            tokenApi = prop.getProperty("authorization");

        } catch (IOException ex) {
            System.out.println("verify your token api in properties file!");
        }
		String urlCreateMultipleDroplets = "https://api.digitalocean.com/v2/";// droplets   /v2/droplets?tag_name=$TAG_NAME
		Client createDropletsRestClient = ClientBuilder.newClient();
		WebTarget baseTarget = createDropletsRestClient.target(urlCreateMultipleDroplets);	
		WebTarget tagDroplet = baseTarget.queryParam("tag_name", tag).path("droplets");
		Response DeleteResponse =  tagDroplet.request()
				.header("Authorization", "Bearer " + tokenApi)
				.delete();
		System.out.println("Status = " + DeleteResponse.getStatus());
		if (DeleteResponse.getStatus() == 204 )
			System.out.println("Droplets Deleted Successfuly");
		
	}
	

	@Override
	@POST
	
	public List<Droplet> addDroplets(DropletsCreationPOJO dropletsToBeCreated) {
		String tokenApi="";
		try (InputStream input = new FileInputStream("parameters.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            tokenApi = prop.getProperty("authorization");

        } catch (IOException ex) {
            System.out.println("verify your token api in properties file!");
        }
		List<Droplet> addedDroplets = new ArrayList<Droplet>();
		String urlCreateMultipleDroplets = "https://api.digitalocean.com/v2/";// droplets

		Client createDropletsRestClient = ClientBuilder.newClient();
		WebTarget baseTarget = createDropletsRestClient.target(urlCreateMultipleDroplets);
		WebTarget dropletsTarget = baseTarget.path("droplets");
		Response postResponse =  dropletsTarget.request()
				.header("Authorization", "Bearer " + tokenApi)
				.post(Entity.entity(dropletsToBeCreated, "application/json"));
		if(postResponse.getStatus() == 202) System.out.println("Droplets added successfully!");
		return addedDroplets;
	}

	
	
	
	
	
	@Override
	public void displayDroplets() throws FileNotFoundException, IOException {
		String tokenApi="";
		try (InputStream input = new FileInputStream("parameters.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            tokenApi = prop.getProperty("authorization");

        } catch (IOException ex) {
            System.out.println("verify your token api in properties file!");
        }
		String urlCreateMultipleDroplets = "https://api.digitalocean.com/v2/";// droplets
		Client createDropletsRestClient = ClientBuilder.newClient();
		WebTarget baseTarget = createDropletsRestClient.target(urlCreateMultipleDroplets);
		WebTarget dropletsTarget = baseTarget.path("droplets");
		Response getResponse =  dropletsTarget.request()
				.header("Authorization", "Bearer " + tokenApi)
				.get(Response.class);
		String jsonString = getResponse.readEntity(String.class);
		ParseJSONPOSTResponse.parseJson(jsonString);
	}

	
	public static <K extends Object, V extends Object> Map<K, V> getJsonAsMap(String json) {
	    try {
	      ObjectMapper mapper = new ObjectMapper();
	      TypeReference<Map<K, V>> typeRef = new TypeReference<Map<K, V>>() {
	      };
	      return mapper.readValue(json, typeRef);
	    } catch (Exception e) {
	      throw new RuntimeException("Couldn't parse json:" + json);
	    }
	  }
	
	
	
}
