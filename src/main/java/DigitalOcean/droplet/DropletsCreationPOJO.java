package DigitalOcean.droplet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DropletsCreationPOJO implements Serializable {
	private List<String> names; // Required
	private String region; // Required
	private String size; // Required
	//private int image_id;// Required
	// Droplet creation will need either image id or image slag and passe it into
	// image attribute
	private String image;
	private List<String> ssh_keys; // "ssh_keys":[{"id":26129207,"fingerprint":"11:69:8e:2c:d4:a2:62:1f:47:d3:8d:43:72:b6:a1:8e","public_key":"ssh-rsa
	// AAAAB3NzaC1yc2EAAAABJQAAAQEA5t95+xORkZHae1llFeSLg/L64+OlIDVl0dAKLYQRxgALnjw7WJ071D/NN2ytH7lnzwM4rMVykXtlQWgS3H7MoJNtwGFIbSxQyO3KyMpJsQ36hvf8nRqzjM0T062EWIEWo2CgQ0/lHwY7wmslfVd2f1j4yNG2DNQVOqB8bwF2gAm510KmdhkoqKqQq7OznKBvJMwBzB3dH5h9pyuWJDCKFcLW6Cd2uSVpnVT2pd4msoD3YCetEZ7TGjKQrpS7X5+QdbxNAdnWvxZ/LIJyVYKAl1ad9ktusWidyWS894biJq8HOOEJJcQbDswHvlogQPPrg3Y007ImX+3VzuVas3QGbQ==
	// rsa-key-20190618","name":"key"}],"links":{},"meta":{"total":1}
	private boolean backups;
	
	private boolean ipv6;
	private boolean private_networking;
	private String user_data;
	private boolean monitoring;
	private List<String> tags;
	
	

	public DropletsCreationPOJO() {
		super();
	}

	public DropletsCreationPOJO(List<String> names, String region, String size, String image_slug,
			boolean backups, boolean ipv6, boolean private_networking, String user_data,
			boolean monitoring, List<String> tags) {
		super();
		this.names = names;
		this.region = region;
		this.size = size;
		this.image = image_slug;
		this.ssh_keys = this.getSsh_keys();
		this.backups = backups;
		
		this.ipv6 = ipv6;
		this.private_networking = private_networking;
		this.user_data = user_data;
		this.monitoring = monitoring;
		this.tags = tags;
	}

	public List<String> getSsh_keys() {
		List<String> ssh_keys_fingerPrints = new ArrayList<String>();
		ssh_keys_fingerPrints.add("33:8a:c5:0a:f4:26:5c:01:a6:76:14:d8:22:5b:e6:f6");
		//TODO Read SSH Key from API
		return ssh_keys_fingerPrints;
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}




	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isBackups() {
		return backups;
	}

	public void setBackups(boolean backups) {
		this.backups = backups;
	}

	public boolean isIpv6() {
		return ipv6;
	}

	public void setIpv6(boolean ipv6) {
		this.ipv6 = ipv6;
	}

	public boolean isPrivate_networking() {
		return private_networking;
	}

	public void setPrivate_networking(boolean private_networking) {
		this.private_networking = private_networking;
	}

	public String getUser_data() {
		return user_data;
	}

	public void setUser_data(String user_data) {
		this.user_data = user_data;
	}

	public boolean isMonitoring() {
		return monitoring;
	}

	public void setMonitoring(boolean monitoring) {
		this.monitoring = monitoring;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "DropletsCreationPOJO [names=" + names + ", region=" + region + ", size=" + size + ", image_slug="
				+ image + ", ssh_keys=" + ssh_keys + ", backups=" + backups + ", ipv6=" + ipv6
				+ ", private_networking=" + private_networking + ", user_data=" + user_data + ", monitoring="
				+ monitoring + ", tags=" + tags + "]";
	}

	

}
