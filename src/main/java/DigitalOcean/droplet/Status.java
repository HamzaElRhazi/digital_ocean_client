package DigitalOcean.droplet;

public enum Status {
	NEW,//TODO NEW should be transleted into new when used as droplet attribute.. 
	//after each use of Status, test if the value is NEW, if it is the case, replace it with 'new'
	active,
	off,
	archive
}
