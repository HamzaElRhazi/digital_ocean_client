package DigitalOcean.droplet;

import java.math.BigInteger;
import java.util.List;
/*
 * This will be used to retreive Droplets info
 */

public class Droplet {
	private int id;
	private String name;
	private int memory;
	private int vcpus;
	private int disk;
	private boolean locked;//this will be initially set at true. When the droplet become successfully created, locked = false.
	private String created_at;
	private Status status;//will be initially set at new. When the droplet become successfully created, status = active.
	private List<Integer> backup_ids;//enabled at the time of creation 	
	private List<Integer> snapshot_ids;
	private List<String> features;
	private Object region; //find more in the specific section od the API doc
	private Object image; //find more in the specific section od the API doc
	private Object size; 
	private String size_slug; 
	private Object networks;
	private Object kernel; //nullable object, This will initially be set to the kernel of the base image when the Droplet is created
	private Object next_backup_window;
	private List<String> tags;
	private List<Integer> volume_ids;//type Integer to be verified
	
	
	
	public Droplet() {
		super();
	}
	public Droplet(int id, String name, int memory, int vcpus, int disk, boolean locked, String created_at,
			Status status, List<Integer> backup_ids, List<Integer> snapshot_ids, List<String> features, Object region,
			Object image, Object size, String size_slug, Object networks, Object kernel, Object next_backup_window,
			List<String> tags, List<Integer> volume_ids) {
		super();
		this.id = id;
		this.name = name;
		this.memory = memory;
		this.vcpus = vcpus;
		this.disk = disk;
		this.locked = locked;
		this.created_at = created_at;
		this.status = status;
		this.backup_ids = backup_ids;
		this.snapshot_ids = snapshot_ids;
		this.features = features;
		this.region = region;
		this.image = image;
		this.size = size;
		this.size_slug = size_slug;
		this.networks = networks;
		this.kernel = kernel;
		this.next_backup_window = next_backup_window;
		this.tags = tags;
		this.volume_ids = volume_ids;
	}
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMemory() {
		return memory;
	}
	public void setMemory(int memory) {
		this.memory = memory;
	}
	public int getVcpus() {
		return vcpus;
	}
	public void setVcpus(int vcpus) {
		this.vcpus = vcpus;
	}
	public int getDisk() {
		return disk;
	}
	public void setDisk(int disk) {
		this.disk = disk;
	}
	public boolean isLocked() {
		return locked;
	}
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public List<Integer> getBackup_ids() {
		return backup_ids;
	}
	public void setBackup_ids(List<Integer> backup_ids) {
		this.backup_ids = backup_ids;
	}
	public List<Integer> getSnapshot_ids() {
		return snapshot_ids;
	}
	public void setSnapshot_ids(List<Integer> snapshot_ids) {
		this.snapshot_ids = snapshot_ids;
	}
	public List<String> getFeatures() {
		return features;
	}
	public void setFeatures(List<String> features) {
		this.features = features;
	}
	public Object getRegion() {
		return region;
	}
	public void setRegion(Object region) {
		this.region = region;
	}
	public Object getImage() {
		return image;
	}
	public void setImage(Object image) {
		this.image = image;
	}
	public Object getSize() {
		return size;
	}
	public void setSize(Object size) {
		this.size = size;
	}
	public String getSize_slug() {
		return size_slug;
	}
	public void setSize_slug(String size_slug) {
		this.size_slug = size_slug;
	}
	public Object getNetworks() {
		return networks;
	}
	public void setNetworks(Object networks) {
		this.networks = networks;
	}
	public Object getKernel() {
		return kernel;
	}
	public void setKernel(Object kernel) {
		this.kernel = kernel;
	}
	public Object getNext_backup_window() {
		return next_backup_window;
	}
	public void setNext_backup_window(Object next_backup_window) {
		this.next_backup_window = next_backup_window;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public List<Integer> getVolume_ids() {
		return volume_ids;
	}
	public void setVolume_ids(List<Integer> volume_ids) {
		this.volume_ids = volume_ids;
	}
	@Override
	public String toString() {
		return "Droplet [id=" + id + ", name=" + name + ", memory=" + memory + ", vcpus=" + vcpus + ", disk=" + disk
				+ ", locked=" + locked + ", created_at=" + created_at + ", status=" + status + ", backup_ids="
				+ backup_ids + ", snapshot_ids=" + snapshot_ids + ", features=" + features + ", region=" + region
				+ ", image=" + image + ", size=" + size + ", size_slug=" + size_slug + ", networks=" + networks
				+ ", kernel=" + kernel + ", next_backup_window=" + next_backup_window + ", tags=" + tags
				+ ", volume_ids=" + volume_ids + "]";
	}
	
}
