package DigitalOcean.Digital_Ocean_Client;

import static java.lang.System.exit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import DigitalOcean.droplet.DropletsServices;
import DigitalOcean.droplet.ParseJSONPOSTResponse;

/**
 * Hello world!
 *
 */

public class App {
	private static DropletsServices dropletsServices;

	public static Map<Integer, Integer> mapDropletsAvailableToDeletion = new HashMap<>();

	public static void main(String[] args) throws FileNotFoundException, IOException {		
		menu();
		
		// dropletsServices = new DropletsServices();
		// dropletsServices.addDroplets(dropletsServices.prepareDropletsToAdd());
		// dropletsServices = new DropletsServices();dropletsServices.displayDroplets();
	}
	

	public static void menu() {
		System.out.println("--------------------------- Menu ---------------------------\n\n");
		System.out.println("1- Add one or many droplets simultaneously");
		System.out.println("2- Delete Droplet b ID");
		System.out.println("3- Delete Droplets by tag");
		System.out.println("4- Display Available Droplets");// TODO the servers with their IP should be shown on the
		System.out.println("5- Exit");

		System.out.print("Type your choice and hit Enter to continue: ");

		int chooseFunction = 0;
		Scanner chooseFunctionReader = new Scanner(System.in);
		try {
			chooseFunction = chooseFunctionReader.nextInt();
			// chooseFunctionReader.close();//this poses a problem
			if (chooseFunction > 0 && chooseFunction <= 5) {
				dropletsServices = new DropletsServices();
				if (chooseFunction == 1) {
					dropletsServices.addDroplets(dropletsServices.prepareDropletsToAdd());// return an array of created
																							// list of Droplets
				} else if (chooseFunction == 2) {
					deleteMenu();

				} else if (chooseFunction == 3) {
					System.out.println("Type the tag : ");
					Scanner tagScanner = new Scanner(System.in);
					String typedTag = tagScanner.next();
					dropletsServices.deleteDropletsByTag(typedTag);
				}
				
				else if (chooseFunction == 4) {
					dropletsServices.displayDroplets();
				} else if (chooseFunction == 5) {
					exit(0);
				}

				menu();

			} else {
				System.out.println("Choice not Valid! ");
				System.out.println("Try again");
				menu();
			}
		} catch (Exception e) {
			System.out.println("Something goes wrong!");
			System.out.println("You have to type an integer value between 1 and 4");
			menu();
		}

	}

	private static void deleteMenu() throws IOException {
		// System.out.println("Please Type the ID of the Droplet you want to delete");
		System.out.println("Here are available droplets in your account :");
		availabeDropletMenu();
		System.out.println("Choose the droplet rang you want to delete");
		int rang;
		Scanner readFileDropletsToBeCreated = new Scanner(System.in);
		rang = readFileDropletsToBeCreated.nextInt();
		Integer chosenId = mapDropletsAvailableToDeletion.get(rang);
		while (chosenId == null) {
			System.out.println("Unvalid provided Rang. Please type available one ");
			chosenId = mapDropletsAvailableToDeletion.get(rang);
		}
		dropletsServices.deleteDroplets(mapDropletsAvailableToDeletion.get(rang));
	}

	private static void availabeDropletMenu() throws FileNotFoundException, IOException {
		String urlCreateMultipleDroplets = "https://api.digitalocean.com/v2/";// droplets
		Client createDropletsRestClient = ClientBuilder.newClient();
		WebTarget baseTarget = createDropletsRestClient.target(urlCreateMultipleDroplets);
		WebTarget dropletsTarget = baseTarget.path("droplets");
		Response getResponse = dropletsTarget.request()
				.header("Authorization", "Bearer " + "f2c973ddb206bc9857a8ecebd8141701a2aaa2f22b7e8c53f7675766ce57de46")
				.get(Response.class);
		String jsonString = getResponse.readEntity(String.class);
		mapDropletsAvailableToDeletion = ParseJSONPOSTResponse.parseJsontoGetAvailableDropletMenu(jsonString);

	}

}
